// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Move spacejpo with arrow keys.
// Chapter 5 spacewar.cpp v1.0
// This class is the core of the game

#include "spaceWar.h"
bool touched = false;
//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
	releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
#pragma region JPO_INITILIZATION
	Game::initialize(hwnd); // throws GameError
	if (!jpoTexture.initialize(graphics,JPO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing jpo texture"));

	if (!jpo.initialize(graphics,JPO_WIDTH, JPO_HEIGHT, JPO_COLS, &jpoTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing jpo"));
	jpo.setX(GAME_WIDTH/4);                    // start above and left of planet
	jpo.setY(GAME_HEIGHT/4);
	jpo.setFrames(JPO_LOOKING_RIGHT_START, JPO_LOOKING_RIGHT_END);   // animation frames
	jpo.setCurrentFrame(JPO_LOOKING_RIGHT_START);     // starting frame
	jpo.setFrameDelay(JPO_ANIMATION_DELAY);
	jpo.setLoop(true);
#pragma endregion

#pragma region BACKGROUND_INITIALIZATION
	if (!backgroundTexture.initialize(graphics,BACKGROUND_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));
	if (!background.initialize(graphics, 0,0,0, &backgroundTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng background image"));
#pragma endregion

#pragma region SNAKE_INITIALIZTION
	if (!snakeTexture1.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake1 texture"));
	if (!snake1.initialize(graphics, 0,0,0, &snakeTexture1))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake1 image"));

	if (!snakeTexture2.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake2 texture"));
	if (!snake2.initialize(graphics, 0,0,0, &snakeTexture2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake2 image"));
	snake2.setX(snake1.getX() + SNAKE_SEG_SIZE);

	if (!snakeTexture3.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake3 texture"));
	if (!snake3.initialize(graphics, 0,0,0, &snakeTexture3))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake3 image"));
	snake3.setX(snake2.getX() + SNAKE_SEG_SIZE);

	if (!snakeTexture4.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake4 texture"));
	if (!snake4.initialize(graphics, 0,0,0, &snakeTexture4))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake4 image"));

	if (!snakeTexture5.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake5 texture"));
	if (!snake5.initialize(graphics, 0,0,0, &snakeTexture5))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake5 image"));

	if (!snakeTexture6.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake6 texture"));
	if (!snake6.initialize(graphics, 0,0,0, &snakeTexture6))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake6 image"));

	if (!snakeTexture7.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake7 texture"));
	if (!snake7.initialize(graphics, 0,0,0, &snakeTexture7))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake7 image"));

	if (!snakeTexture8.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake8 texture"));
	if (!snake8.initialize(graphics, 0,0,0, &snakeTexture8))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake8 image"));

	if (!snakeTexture9.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake9 texture"));
	if (!snake9.initialize(graphics, 0,0,0, &snakeTexture9))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake9 image"));

	if (!snakeTexture10.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake10 texture"));
	if (!snake10.initialize(graphics, 0,0,0, &snakeTexture10))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake10 image"));

	if (!snakeTexture11.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake11 texture"));
	if (!snake11.initialize(graphics, 0,0,0, &snakeTexture11))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake11 image"));

	if (!snakeTexture12.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake12 texture"));
	if (!snake12.initialize(graphics, 0,0,0, &snakeTexture12))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake12 image"));

	if (!snakeTexture13.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake13 texture"));
	if (!snake13.initialize(graphics, 0,0,0, &snakeTexture13))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake13 image"));

	if (!snakeTexture14.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake14 texture"));
	if (!snake14.initialize(graphics, 0,0,0, &snakeTexture14))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake14 image"));

	if (!snakeTexture15.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake15 texture"));
	if (!snake15.initialize(graphics, 0,0,0, &snakeTexture15))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake15 image"));

	if (!snakeTexture16.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake16 texture"));
	if (!snake16.initialize(graphics, 0,0,0, &snakeTexture16))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake16 image"));

	if (!snakeTexture17.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake17 texture"));
	if (!snake17.initialize(graphics, 0,0,0, &snakeTexture17))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake17 image"));

	if (!snakeTexture18.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake18 texture"));
	if (!snake18.initialize(graphics, 0,0,0, &snakeTexture18))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake18 image"));

	if (!snakeTexture19.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake19 texture"));
	if (!snake19.initialize(graphics, 0,0,0, &snakeTexture19))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake19 image"));

	if (!snakeTexture20.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake20 texture"));
	if (!snake20.initialize(graphics, 0,0,0, &snakeTexture20))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake20 image"));

	if (!snakeTexture21.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake21 texture"));
	if (!snake21.initialize(graphics, 0,0,0, &snakeTexture21))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake21 image"));

	if (!snakeTexture22.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake22 texture"));
	if (!snake22.initialize(graphics, 0,0,0, &snakeTexture22))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake22 image"));

	if (!snakeTexture23.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake23 texture"));
	if (!snake23.initialize(graphics, 0,0,0, &snakeTexture23))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake23 image"));

	if (!snakeTexture24.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake24 texture"));
	if (!snake24.initialize(graphics, 0,0,0, &snakeTexture24))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake24 image"));

	if (!snakeTexture25.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake25 texture"));
	if (!snake25.initialize(graphics, 0,0,0, &snakeTexture25))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake25 image"));

	if (!snakeTexture26.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake26 texture"));
	if (!snake26.initialize(graphics, 0,0,0, &snakeTexture26))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake26 image"));

	if (!snakeTexture27.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake27 texture"));
	if (!snake27.initialize(graphics, 0,0,0, &snakeTexture27))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake27 image"));

	if (!snakeTexture28.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake28 texture"));
	if (!snake28.initialize(graphics, 0,0,0, &snakeTexture28))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake28 image"));

	if (!snakeTexture29.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake29 texture"));
	if (!snake29.initialize(graphics, 0,0,0, &snakeTexture29))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake29 image"));

	if (!snakeTexture30.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake30 texture"));
	if (!snake30.initialize(graphics, 0,0,0, &snakeTexture30))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake30 image"));

	if (!snakeTexture31.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake31 texture"));
	if (!snake31.initialize(graphics, 0,0,0, &snakeTexture31))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake31 image"));

	if (!snakeTexture32.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake32 texture"));
	if (!snake32.initialize(graphics, 0,0,0, &snakeTexture32))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake32 image"));

	if (!snakeTexture33.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake33 texture"));
	if (!snake33.initialize(graphics, 0,0,0, &snakeTexture33))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake33 image"));

	if (!snakeTexture34.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake34 texture"));
	if (!snake34.initialize(graphics, 0,0,0, &snakeTexture34))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake34 image"));

	if (!snakeTexture35.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake35 texture"));
	if (!snake35.initialize(graphics, 0,0,0, &snakeTexture35))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake35 image"));

	if (!snakeTexture36.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake36 texture"));
	if (!snake36.initialize(graphics, 0,0,0, &snakeTexture36))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake36 image"));

	if (!snakeTexture37.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake37 texture"));
	if (!snake37.initialize(graphics, 0,0,0, &snakeTexture37))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake37 image"));

	if (!snakeTexture38.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake38 texture"));
	if (!snake38.initialize(graphics, 0,0,0, &snakeTexture38))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake38 image"));

	if (!snakeTexture39.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake39 texture"));
	if (!snake39.initialize(graphics, 0,0,0, &snakeTexture39))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake39 image"));

	if (!snakeTexture40.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake40 texture"));
	if (!snake40.initialize(graphics, 0,0,0, &snakeTexture40))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake40 image"));

	if (!snakeTexture41.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake41 texture"));
	if (!snake41.initialize(graphics, 0,0,0, &snakeTexture41))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake41 image"));

	if (!snakeTexture42.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake42 texture"));
	if (!snake42.initialize(graphics, 0,0,0, &snakeTexture42))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake42 image"));

	if (!snakeTexture43.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake43 texture"));
	if (!snake43.initialize(graphics, 0,0,0, &snakeTexture43))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake43 image"));

	if (!snakeTexture44.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake44 texture"));
	if (!snake44.initialize(graphics, 0,0,0, &snakeTexture44))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake44 image"));

	if (!snakeTexture45.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake45 texture"));
	if (!snake45.initialize(graphics, 0,0,0, &snakeTexture45))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake45 image"));

	if (!snakeTexture46.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake46 texture"));
	if (!snake46.initialize(graphics, 0,0,0, &snakeTexture46))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake46 image"));

	if (!snakeTexture47.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake47 texture"));
	if (!snake47.initialize(graphics, 0,0,0, &snakeTexture47))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake47 image"));

	if (!snakeTexture48.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake48 texture"));
	if (!snake48.initialize(graphics, 0,0,0, &snakeTexture48))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake48 image"));

	if (!snakeTexture49.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake49 texture"));
	if (!snake49.initialize(graphics, 0,0,0, &snakeTexture49))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake49 image"));

	if (!snakeTexture50.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake50 texture"));
	if (!snake50.initialize(graphics, 0,0,0, &snakeTexture50))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake50 image"));

	if (!snakeTexture51.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake51 texture"));
	if (!snake51.initialize(graphics, 0,0,0, &snakeTexture51))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake51 image"));

	if (!snakeTexture52.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake52 texture"));
	if (!snake52.initialize(graphics, 0,0,0, &snakeTexture52))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake52 image"));

	if (!snakeTexture53.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake53 texture"));
	if (!snake53.initialize(graphics, 0,0,0, &snakeTexture53))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake53 image"));

	if (!snakeTexture54.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake54 texture"));
	if (!snake54.initialize(graphics, 0,0,0, &snakeTexture54))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake54 image"));

	if (!snakeTexture55.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake55 texture"));
	if (!snake55.initialize(graphics, 0,0,0, &snakeTexture55))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake55 image"));

	if (!snakeTexture56.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake56 texture"));
	if (!snake56.initialize(graphics, 0,0,0, &snakeTexture56))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake56 image"));

	if (!snakeTexture57.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake57 texture"));
	if (!snake57.initialize(graphics, 0,0,0, &snakeTexture57))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake57 image"));

	if (!snakeTexture58.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake58 texture"));
	if (!snake58.initialize(graphics, 0,0,0, &snakeTexture58))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake58 image"));

	if (!snakeTexture59.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake59 texture"));
	if (!snake59.initialize(graphics, 0,0,0, &snakeTexture59))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake59 image"));

	if (!snakeTexture60.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake60 texture"));
	if (!snake60.initialize(graphics, 0,0,0, &snakeTexture60))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake60 image"));

	if (!snakeTexture61.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake61 texture"));
	if (!snake61.initialize(graphics, 0,0,0, &snakeTexture61))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake61 image"));

	if (!snakeTexture62.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake62 texture"));
	if (!snake62.initialize(graphics, 0,0,0, &snakeTexture62))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake62 image"));

	if (!snakeTexture63.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake63 texture"));
	if (!snake63.initialize(graphics, 0,0,0, &snakeTexture63))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake63 image"));

	if (!snakeTexture64.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake64 texture"));
	if (!snake64.initialize(graphics, 0,0,0, &snakeTexture64))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake64 image"));

	if (!snakeTexture65.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake65 texture"));
	if (!snake65.initialize(graphics, 0,0,0, &snakeTexture65))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake65 image"));

	if (!snakeTexture66.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake66 texture"));
	if (!snake66.initialize(graphics, 0,0,0, &snakeTexture66))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake66 image"));

	if (!snakeTexture67.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake67 texture"));
	if (!snake67.initialize(graphics, 0,0,0, &snakeTexture67))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake67 image"));

	if (!snakeTexture68.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake68 texture"));
	if (!snake68.initialize(graphics, 0,0,0, &snakeTexture68))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake68 image"));

	if (!snakeTexture69.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake69 texture"));
	if (!snake69.initialize(graphics, 0,0,0, &snakeTexture69))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake69 image"));

	if (!snakeTexture70.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake70 texture"));
	if (!snake70.initialize(graphics, 0,0,0, &snakeTexture70))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake70 image"));

	if (!snakeTexture71.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake71 texture"));
	if (!snake71.initialize(graphics, 0,0,0, &snakeTexture71))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake71 image"));

	if (!snakeTexture72.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake72 texture"));
	if (!snake72.initialize(graphics, 0,0,0, &snakeTexture72))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake72 image"));

	if (!snakeTexture73.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake73 texture"));
	if (!snake73.initialize(graphics, 0,0,0, &snakeTexture73))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake73 image"));

	if (!snakeTexture74.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake74 texture"));
	if (!snake74.initialize(graphics, 0,0,0, &snakeTexture74))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake74 image"));

	if (!snakeTexture75.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake75 texture"));
	if (!snake75.initialize(graphics, 0,0,0, &snakeTexture75))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake75 image"));

	if (!snakeTexture76.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake76 texture"));
	if (!snake76.initialize(graphics, 0,0,0, &snakeTexture76))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake76 image"));

	if (!snakeTexture77.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake77 texture"));
	if (!snake77.initialize(graphics, 0,0,0, &snakeTexture77))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake77 image"));

	if (!snakeTexture78.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake78 texture"));
	if (!snake78.initialize(graphics, 0,0,0, &snakeTexture78))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake78 image"));

	if (!snakeTexture79.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake79 texture"));
	if (!snake79.initialize(graphics, 0,0,0, &snakeTexture79))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake79 image"));

	if (!snakeTexture80.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake80 texture"));
	if (!snake80.initialize(graphics, 0,0,0, &snakeTexture80))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake80 image"));

	if (!snakeTexture81.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake81 texture"));
	if (!snake81.initialize(graphics, 0,0,0, &snakeTexture81))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake81 image"));

	if (!snakeTexture82.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake82 texture"));
	if (!snake82.initialize(graphics, 0,0,0, &snakeTexture82))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake82 image"));

	if (!snakeTexture83.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake83 texture"));
	if (!snake83.initialize(graphics, 0,0,0, &snakeTexture83))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake83 image"));

	if (!snakeTexture84.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake84 texture"));
	if (!snake84.initialize(graphics, 0,0,0, &snakeTexture84))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake84 image"));

	if (!snakeTexture85.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake85 texture"));
	if (!snake85.initialize(graphics, 0,0,0, &snakeTexture85))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake85 image"));

	if (!snakeTexture86.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake86 texture"));
	if (!snake86.initialize(graphics, 0,0,0, &snakeTexture86))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake86 image"));

	if (!snakeTexture87.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake87 texture"));
	if (!snake87.initialize(graphics, 0,0,0, &snakeTexture87))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake87 image"));

	if (!snakeTexture88.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake88 texture"));
	if (!snake88.initialize(graphics, 0,0,0, &snakeTexture88))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake88 image"));

	if (!snakeTexture89.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake89 texture"));
	if (!snake89.initialize(graphics, 0,0,0, &snakeTexture89))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake89 image"));

	if (!snakeTexture90.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake90 texture"));
	if (!snake90.initialize(graphics, 0,0,0, &snakeTexture90))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake90 image"));

	if (!snakeTexture91.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake91 texture"));
	if (!snake91.initialize(graphics, 0,0,0, &snakeTexture91))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake91 image"));

	if (!snakeTexture92.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake92 texture"));
	if (!snake92.initialize(graphics, 0,0,0, &snakeTexture92))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake92 image"));

	if (!snakeTexture93.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake93 texture"));
	if (!snake93.initialize(graphics, 0,0,0, &snakeTexture93))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake93 image"));

	if (!snakeTexture94.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake94 texture"));
	if (!snake94.initialize(graphics, 0,0,0, &snakeTexture94))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake94 image"));

	if (!snakeTexture95.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake95 texture"));
	if (!snake95.initialize(graphics, 0,0,0, &snakeTexture95))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake95 image"));

	if (!snakeTexture96.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake96 texture"));
	if (!snake96.initialize(graphics, 0,0,0, &snakeTexture96))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake96 image"));

	if (!snakeTexture97.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake97 texture"));
	if (!snake97.initialize(graphics, 0,0,0, &snakeTexture97))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake97 image"));

	if (!snakeTexture98.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake98 texture"));
	if (!snake98.initialize(graphics, 0,0,0, &snakeTexture98))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake98 image"));

	if (!snakeTexture99.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake99 texture"));
	if (!snake99.initialize(graphics, 0,0,0, &snakeTexture99))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake99 image"));

	if (!snakeTexture100.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake100 texture"));
	if (!snake100.initialize(graphics, 0,0,0, &snakeTexture100))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake100 image"));

	if (!snakeTexture101.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake101 texture"));
	if (!snake101.initialize(graphics, 0,0,0, &snakeTexture101))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake101 image"));

	if (!snakeTexture102.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake102 texture"));
	if (!snake102.initialize(graphics, 0,0,0, &snakeTexture102))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake102 image"));

	if (!snakeTexture103.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake103 texture"));
	if (!snake103.initialize(graphics, 0,0,0, &snakeTexture103))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake103 image"));

	if (!snakeTexture104.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake104 texture"));
	if (!snake104.initialize(graphics, 0,0,0, &snakeTexture104))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake104 image"));

	if (!snakeTexture105.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake105 texture"));
	if (!snake105.initialize(graphics, 0,0,0, &snakeTexture105))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake105 image"));

	if (!snakeTexture106.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake106 texture"));
	if (!snake106.initialize(graphics, 0,0,0, &snakeTexture106))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake106 image"));

	if (!snakeTexture107.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake107 texture"));
	if (!snake107.initialize(graphics, 0,0,0, &snakeTexture107))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake107 image"));

	if (!snakeTexture108.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake108 texture"));
	if (!snake108.initialize(graphics, 0,0,0, &snakeTexture108))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake108 image"));

	if (!snakeTexture109.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake109 texture"));
	if (!snake109.initialize(graphics, 0,0,0, &snakeTexture109))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake109 image"));

	if (!snakeTexture110.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake110 texture"));
	if (!snake110.initialize(graphics, 0,0,0, &snakeTexture110))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake110 image"));

	if (!snakeTexture111.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake111 texture"));
	if (!snake111.initialize(graphics, 0,0,0, &snakeTexture111))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake111 image"));

	if (!snakeTexture112.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake112 texture"));
	if (!snake112.initialize(graphics, 0,0,0, &snakeTexture112))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake112 image"));

	if (!snakeTexture113.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake113 texture"));
	if (!snake113.initialize(graphics, 0,0,0, &snakeTexture113))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake113 image"));

	if (!snakeTexture114.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake114 texture"));
	if (!snake114.initialize(graphics, 0,0,0, &snakeTexture114))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake114 image"));

	if (!snakeTexture115.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake115 texture"));
	if (!snake115.initialize(graphics, 0,0,0, &snakeTexture115))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake115 image"));

	if (!snakeTexture116.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake116 texture"));
	if (!snake116.initialize(graphics, 0,0,0, &snakeTexture116))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake116 image"));

	if (!snakeTexture117.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake117 texture"));
	if (!snake117.initialize(graphics, 0,0,0, &snakeTexture117))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake117 image"));

	if (!snakeTexture118.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake118 texture"));
	if (!snake118.initialize(graphics, 0,0,0, &snakeTexture118))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake118 image"));

	if (!snakeTexture119.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake119 texture"));
	if (!snake119.initialize(graphics, 0,0,0, &snakeTexture119))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake119 image"));

	if (!snakeTexture120.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake120 texture"));
	if (!snake120.initialize(graphics, 0,0,0, &snakeTexture120))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake120 image"));

	if (!snakeTexture121.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake121 texture"));
	if (!snake121.initialize(graphics, 0,0,0, &snakeTexture121))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake121 image"));

	if (!snakeTexture122.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake122 texture"));
	if (!snake122.initialize(graphics, 0,0,0, &snakeTexture122))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake122 image"));

	if (!snakeTexture123.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake123 texture"));
	if (!snake123.initialize(graphics, 0,0,0, &snakeTexture123))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake123 image"));

	if (!snakeTexture124.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake124 texture"));
	if (!snake124.initialize(graphics, 0,0,0, &snakeTexture124))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake124 image"));

	if (!snakeTexture125.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake125 texture"));
	if (!snake125.initialize(graphics, 0,0,0, &snakeTexture125))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake125 image"));

	if (!snakeTexture126.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake126 texture"));
	if (!snake126.initialize(graphics, 0,0,0, &snakeTexture126))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake126 image"));

	if (!snakeTexture127.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake127 texture"));
	if (!snake127.initialize(graphics, 0,0,0, &snakeTexture127))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake127 image"));

	if (!snakeTexture128.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake128 texture"));
	if (!snake128.initialize(graphics, 0,0,0, &snakeTexture128))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake128 image"));

	if (!snakeTexture129.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake129 texture"));
	if (!snake129.initialize(graphics, 0,0,0, &snakeTexture129))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake129 image"));

	if (!snakeTexture130.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake130 texture"));
	if (!snake130.initialize(graphics, 0,0,0, &snakeTexture130))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake130 image"));

	if (!snakeTexture131.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake131 texture"));
	if (!snake131.initialize(graphics, 0,0,0, &snakeTexture131))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake131 image"));

	if (!snakeTexture132.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake132 texture"));
	if (!snake132.initialize(graphics, 0,0,0, &snakeTexture132))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake132 image"));

	if (!snakeTexture133.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake133 texture"));
	if (!snake133.initialize(graphics, 0,0,0, &snakeTexture133))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake133 image"));

	if (!snakeTexture134.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake134 texture"));
	if (!snake134.initialize(graphics, 0,0,0, &snakeTexture134))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake134 image"));

	if (!snakeTexture135.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake135 texture"));
	if (!snake135.initialize(graphics, 0,0,0, &snakeTexture135))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake135 image"));

	if (!snakeTexture136.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake136 texture"));
	if (!snake136.initialize(graphics, 0,0,0, &snakeTexture136))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake136 image"));

	if (!snakeTexture137.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake137 texture"));
	if (!snake137.initialize(graphics, 0,0,0, &snakeTexture137))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake137 image"));

	if (!snakeTexture138.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake138 texture"));
	if (!snake138.initialize(graphics, 0,0,0, &snakeTexture138))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake138 image"));

	if (!snakeTexture139.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake139 texture"));
	if (!snake139.initialize(graphics, 0,0,0, &snakeTexture139))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake139 image"));

	if (!snakeTexture140.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake140 texture"));
	if (!snake140.initialize(graphics, 0,0,0, &snakeTexture140))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake140 image"));

	if (!snakeTexture141.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake141 texture"));
	if (!snake141.initialize(graphics, 0,0,0, &snakeTexture141))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake141 image"));

	if (!snakeTexture142.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake142 texture"));
	if (!snake142.initialize(graphics, 0,0,0, &snakeTexture142))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake142 image"));

	if (!snakeTexture143.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake143 texture"));
	if (!snake143.initialize(graphics, 0,0,0, &snakeTexture143))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake143 image"));

	if (!snakeTexture144.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake144 texture"));
	if (!snake144.initialize(graphics, 0,0,0, &snakeTexture144))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake144 image"));

	if (!snakeTexture145.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake145 texture"));
	if (!snake145.initialize(graphics, 0,0,0, &snakeTexture145))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake145 image"));

	if (!snakeTexture146.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake146 texture"));
	if (!snake146.initialize(graphics, 0,0,0, &snakeTexture146))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake146 image"));

	if (!snakeTexture147.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake147 texture"));
	if (!snake147.initialize(graphics, 0,0,0, &snakeTexture147))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake147 image"));

	if (!snakeTexture148.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake148 texture"));
	if (!snake148.initialize(graphics, 0,0,0, &snakeTexture148))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake148 image"));

	if (!snakeTexture149.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake149 texture"));
	if (!snake149.initialize(graphics, 0,0,0, &snakeTexture149))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake149 image"));

	if (!snakeTexture150.initialize(graphics,SNAKE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing snake150 texture"));
	if (!snake150.initialize(graphics, 0,0,0, &snakeTexture150))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializng snake150 image"));
#pragma endregion

#pragma region RAT_INITIALIZATION
	//initialize the rat
	if (!ratTexture.initialize(graphics,RAT_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing rat texture"));
	if (!rat.initialize(graphics,RAT_WIDTH, RAT_HEIGHT, RAT_COLS, &ratTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing rat image"));
	rat.setX(500);                    // start above and left of planet
	rat.setY(500);
	rat.setFrames(RAT_IDLE_START, RAT_IDLE_END);   // animation frames
	rat.setCurrentFrame(RAT_IDLE_START);     // starting frame
	rat.setFrameDelay(RAT_ANIMATION_DELAY);
	rat.setLoop(true);
#pragma endregion

	direction = right;
	keyDownLastFrame = false;
	keyDownThisFrame = false;
	snakeSize = 3; //starting at size 3
	snakeBody.push(&snake1);
	snakeBody.push(&snake2);
	snakeBody.push(&snake3);
	tailHolder = &snake1;
	headHolder = &snake1;
	timer = 0;

	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{

	if(input->isKeyDown(JPO_RIGHT_KEY) && touched == false && direction != left)            // if move right, can't go from left to right
	{
		//touched = true;
		keyDownThisFrame = true;
		direction = right;
	}
	if(input->isKeyDown(JPO_LEFT_KEY) && touched == false && direction != right)             // if move left, can't go from right to left
	{
		//touched = true;
		direction = left;
		keyDownThisFrame = true;
	}
	if(input->isKeyDown(JPO_UP_KEY) && touched == false && direction != down)               // if move up, can't go from down to up
	{
		//touched = true;
		keyDownThisFrame = true;
		direction = up;
	}
	if(input->isKeyDown(JPO_DOWN_KEY) && touched == false && direction != up)             // if move down, can't from up to down
	{
		//touched = true;
		keyDownThisFrame = true;
		direction = down;
	}
	//to reset the movement press the space bar
	if (input->isKeyDown(VK_SPACE))
	{
		touched = false;
	}

#pragma region Birmingham_Animation_Code
	/*if (!keyDownLastFrame && keyDownThisFrame)
	{
	switch (Direction)
	{
	case right:
	jpo.setFrames(JPO_WALKING_RIGHT_START, JPO_WALKING_RIGHT_END);
	jpo.setCurrentFrame(JPO_WALKING_RIGHT_START);
	break;
	case left:
	jpo.setCurrentFrame(JPO_WALKING_LEFT_START);
	jpo.setFrames(JPO_WALKING_LEFT_START, JPO_WALKING_LEFT_END);
	break;
	}
	}
	if (!keyDownThisFrame)
	{
	switch (Direction)
	{
	case right:
	jpo.setFrames(JPO_LOOKING_RIGHT_START, JPO_LOOKING_RIGHT_END);
	jpo.setCurrentFrame(JPO_LOOKING_RIGHT_START);
	break;
	case left:
	jpo.setCurrentFrame(JPO_LOOKING_LEFT_START);
	jpo.setFrames(JPO_LOOKING_LEFT_START, JPO_LOOKING_LEFT_END);
	break;
	}
	}*/
#pragma endregion

	//animate the rat
	rat.setFrames(RAT_IDLE_START, RAT_IDLE_END);
	//rat.setCurrentFrame(RAT_IDLE_START);

	if (timer == 50)
	{
		timer = 0;
		tailHolder = snakeBody.front();
		headHolder = snakeBody.back();
		switch (direction)
		{
		case Spacewar::left:
			if ((*headHolder).getX() > 0)
			{
				(*tailHolder).setX((*headHolder).getX() - SNAKE_SEG_SIZE); //the player is moving left so move the tail to the left of the head
				(*tailHolder).setY((*headHolder).getY()); //the player is moving down so make sure we are at the left height
				snakeBody.pop();//take the tail out of the queue
				snakeBody.push(tailHolder);//move it to the head
			}
			else
			{
				MessageBeep((UINT) -1);
			}
			break;
		case Spacewar::right:
			if ((*headHolder).getX() + SNAKE_SEG_SIZE < GAME_WIDTH)
			{
				(*tailHolder).setX((*headHolder).getX() + SNAKE_SEG_SIZE); //the player is moving right so move the tail to the right of the head
				(*tailHolder).setY((*headHolder).getY()); //the player is moving down so make sure we are at the right height
				snakeBody.pop();//take the tail out of the queue
				snakeBody.push(tailHolder);//move it to the head
			}
			else
			{
				MessageBeep((UINT) -1);
			}
			break;
		case Spacewar::up:
			if ((*headHolder).getY() > 0)
			{
				(*tailHolder).setY((*headHolder).getY() - SNAKE_SEG_SIZE); //the player is moving up so move the tail to the above the head
				(*tailHolder).setX((*headHolder).getX()); //we have to make sure the tail goes directly above the head
				snakeBody.pop();
				snakeBody.push(tailHolder);
			}
			else
			{
				MessageBeep((UINT) -1);
			}
			break;
		case Spacewar::down:
			if ((*headHolder).getY() + SNAKE_SEG_SIZE < GAME_HEIGHT)
			{
				(*tailHolder).setY((*headHolder).getY() + SNAKE_SEG_SIZE); //the player is moving down so move the tail to the below the head
				(*tailHolder).setX((*headHolder).getX()); //we have to make sure the tail goes directly below the head
				snakeBody.pop();
				snakeBody.push(tailHolder);
			}
			else
			{
				MessageBeep((UINT) -1);
			}
			break;
		default:
			break;
		}
	}

	keyDownLastFrame = keyDownThisFrame;
	keyDownThisFrame = false;
	jpo.update(frameTime);
	timer++;
	touched = false;
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
	graphics->spriteBegin();                // begin drawing sprites
	background.draw();                            // add the background to the scene
	snake1.draw();
	snake2.draw();
	snake3.draw();
	rat.draw();
	graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
	backgroundTexture.onLostDevice();
	snakeTexture1.onLostDevice();
	snakeTexture2.onLostDevice();
	snakeTexture3.onLostDevice();
	ratTexture.onLostDevice();
	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
	backgroundTexture.onResetDevice();
	snakeTexture1.onResetDevice();
	snakeTexture2.onResetDevice();
	snakeTexture3.onResetDevice();
	ratTexture.onResetDevice();
	Game::resetAll();
	return;
}
