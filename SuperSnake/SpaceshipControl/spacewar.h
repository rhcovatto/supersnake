// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 5 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include <queue>

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:
	//Make the scale for the two images 1. Just in case we have to change them later
	#define SNAKE_IMAGE_SCALE 1
	#define BACKGROUND_IMAGE_SCALE 1

    TextureManager jpoTexture;     // ship texture
    Image   jpo;                   // ship image

#pragma region MAKE_SNAKE_PARTS
	TextureManager snakeTexture1; 
	Image snake1;	
	TextureManager snakeTexture2; 
	Image snake2;
	TextureManager snakeTexture3; 
	Image snake3;
	TextureManager snakeTexture4; 
	Image snake4;
	TextureManager snakeTexture5; 
	Image snake5;
	TextureManager snakeTexture6; 
	Image snake6;	
	TextureManager snakeTexture7; 
	Image snake7;
	TextureManager snakeTexture8; 
	Image snake8;
	TextureManager snakeTexture9; 
	Image snake9;
	TextureManager snakeTexture10; 
	Image snake10;
	TextureManager snakeTexture11; 
	Image snake11;	
	TextureManager snakeTexture12; 
	Image snake12;
	TextureManager snakeTexture13; 
	Image snake13;
	TextureManager snakeTexture14; 
	Image snake14;
	TextureManager snakeTexture15; 
	Image snake15;
	TextureManager snakeTexture16; 
	Image snake16;	
	TextureManager snakeTexture17; 
	Image snake17;
	TextureManager snakeTexture18; 
	Image snake18;
	TextureManager snakeTexture19; 
	Image snake19;
	TextureManager snakeTexture20; 
	Image snake20;
	TextureManager snakeTexture21; 
	Image snake21;	
	TextureManager snakeTexture22; 
	Image snake22;
	TextureManager snakeTexture23; 
	Image snake23;
	TextureManager snakeTexture24; 
	Image snake24;
	TextureManager snakeTexture25; 
	Image snake25;
	TextureManager snakeTexture26; 
	Image snake26;	
	TextureManager snakeTexture27; 
	Image snake27;
	TextureManager snakeTexture28; 
	Image snake28;
	TextureManager snakeTexture29; 
	Image snake29;
	TextureManager snakeTexture30; 
	Image snake30;
	TextureManager snakeTexture31; 
	Image snake31;	
	TextureManager snakeTexture32; 
	Image snake32;
	TextureManager snakeTexture33; 
	Image snake33;
	TextureManager snakeTexture34; 
	Image snake34;
	TextureManager snakeTexture35; 
	Image snake35;
	TextureManager snakeTexture36; 
	Image snake36;	
	TextureManager snakeTexture37; 
	Image snake37;
	TextureManager snakeTexture38; 
	Image snake38;
	TextureManager snakeTexture39; 
	Image snake39;
	TextureManager snakeTexture40; 
	Image snake40;
	TextureManager snakeTexture41; 
	Image snake41;	
	TextureManager snakeTexture42; 
	Image snake42;
	TextureManager snakeTexture43; 
	Image snake43;
	TextureManager snakeTexture44; 
	Image snake44;
	TextureManager snakeTexture45; 
	Image snake45;
	TextureManager snakeTexture46; 
	Image snake46;	
	TextureManager snakeTexture47; 
	Image snake47;
	TextureManager snakeTexture48; 
	Image snake48;
	TextureManager snakeTexture49; 
	Image snake49;
	TextureManager snakeTexture50; 
	Image snake50;
	TextureManager snakeTexture51; 
	Image snake51;	
	TextureManager snakeTexture52; 
	Image snake52;
	TextureManager snakeTexture53; 
	Image snake53;
	TextureManager snakeTexture54; 
	Image snake54;
	TextureManager snakeTexture55; 
	Image snake55;
	TextureManager snakeTexture56; 
	Image snake56;	
	TextureManager snakeTexture57; 
	Image snake57;
	TextureManager snakeTexture58; 
	Image snake58;
	TextureManager snakeTexture59; 
	Image snake59;
	TextureManager snakeTexture60; 
	Image snake60;
	TextureManager snakeTexture61; 
	Image snake61;	
	TextureManager snakeTexture62; 
	Image snake62;
	TextureManager snakeTexture63; 
	Image snake63;
	TextureManager snakeTexture64; 
	Image snake64;
	TextureManager snakeTexture65; 
	Image snake65;
	TextureManager snakeTexture66; 
	Image snake66;	
	TextureManager snakeTexture67; 
	Image snake67;
	TextureManager snakeTexture68; 
	Image snake68;
	TextureManager snakeTexture69; 
	Image snake69;
	TextureManager snakeTexture70; 
	Image snake70;
	TextureManager snakeTexture71; 
	Image snake71;	
	TextureManager snakeTexture72; 
	Image snake72;
	TextureManager snakeTexture73; 
	Image snake73;
	TextureManager snakeTexture74; 
	Image snake74;
	TextureManager snakeTexture75; 
	Image snake75;
	TextureManager snakeTexture76; 
	Image snake76;	
	TextureManager snakeTexture77; 
	Image snake77;
	TextureManager snakeTexture78; 
	Image snake78;
	TextureManager snakeTexture79; 
	Image snake79;
	TextureManager snakeTexture80; 
	Image snake80;
	TextureManager snakeTexture81; 
	Image snake81;	
	TextureManager snakeTexture82; 
	Image snake82;
	TextureManager snakeTexture83; 
	Image snake83;
	TextureManager snakeTexture84; 
	Image snake84;
	TextureManager snakeTexture85; 
	Image snake85;
	TextureManager snakeTexture86; 
	Image snake86;	
	TextureManager snakeTexture87; 
	Image snake87;
	TextureManager snakeTexture88; 
	Image snake88;
	TextureManager snakeTexture89; 
	Image snake89;
	TextureManager snakeTexture90; 
	Image snake90;
	TextureManager snakeTexture91; 
	Image snake91;	
	TextureManager snakeTexture92; 
	Image snake92;
	TextureManager snakeTexture93; 
	Image snake93;
	TextureManager snakeTexture94; 
	Image snake94;
	TextureManager snakeTexture95; 
	Image snake95;
	TextureManager snakeTexture96; 
	Image snake96;	
	TextureManager snakeTexture97; 
	Image snake97;
	TextureManager snakeTexture98; 
	Image snake98;
	TextureManager snakeTexture99; 
	Image snake99;
	TextureManager snakeTexture100; 
	Image snake100;
	TextureManager snakeTexture101; 
	Image snake101;	
	TextureManager snakeTexture102; 
	Image snake102;
	TextureManager snakeTexture103; 
	Image snake103;
	TextureManager snakeTexture104; 
	Image snake104;
	TextureManager snakeTexture105; 
	Image snake105;
	TextureManager snakeTexture106; 
	Image snake106;	
	TextureManager snakeTexture107; 
	Image snake107;
	TextureManager snakeTexture108; 
	Image snake108;
	TextureManager snakeTexture109; 
	Image snake109;
	TextureManager snakeTexture110; 
	Image snake110;
	TextureManager snakeTexture111; 
	Image snake111;	
	TextureManager snakeTexture112; 
	Image snake112;
	TextureManager snakeTexture113; 
	Image snake113;
	TextureManager snakeTexture114; 
	Image snake114;
	TextureManager snakeTexture115; 
	Image snake115;
	TextureManager snakeTexture116; 
	Image snake116;	
	TextureManager snakeTexture117; 
	Image snake117;
	TextureManager snakeTexture118; 
	Image snake118;
	TextureManager snakeTexture119; 
	Image snake119;
	TextureManager snakeTexture120; 
	Image snake120;
	TextureManager snakeTexture121; 
	Image snake121;	
	TextureManager snakeTexture122; 
	Image snake122;
	TextureManager snakeTexture123; 
	Image snake123;
	TextureManager snakeTexture124; 
	Image snake124;
	TextureManager snakeTexture125; 
	Image snake125;
	TextureManager snakeTexture126; 
	Image snake126;	
	TextureManager snakeTexture127; 
	Image snake127;
	TextureManager snakeTexture128; 
	Image snake128;
	TextureManager snakeTexture129; 
	Image snake129;
	TextureManager snakeTexture130; 
	Image snake130;
	TextureManager snakeTexture131; 
	Image snake131;	
	TextureManager snakeTexture132; 
	Image snake132;
	TextureManager snakeTexture133; 
	Image snake133;
	TextureManager snakeTexture134; 
	Image snake134;
	TextureManager snakeTexture135; 
	Image snake135;
	TextureManager snakeTexture136; 
	Image snake136;	
	TextureManager snakeTexture137; 
	Image snake137;
	TextureManager snakeTexture138; 
	Image snake138;
	TextureManager snakeTexture139; 
	Image snake139;
	TextureManager snakeTexture140; 
	Image snake140;
	TextureManager snakeTexture141; 
	Image snake141;	
	TextureManager snakeTexture142; 
	Image snake142;
	TextureManager snakeTexture143; 
	Image snake143;
	TextureManager snakeTexture144; 
	Image snake144;
	TextureManager snakeTexture145; 
	Image snake145;
	TextureManager snakeTexture146; 
	Image snake146;	
	TextureManager snakeTexture147; 
	Image snake147;
	TextureManager snakeTexture148; 
	Image snake148;
	TextureManager snakeTexture149; 
	Image snake149;
	TextureManager snakeTexture150; 
	Image snake150;
#pragma endregion

	TextureManager backgroundTexture; //background texture
	Image background;				  //background image

	enum direction {left, right, up, down} direction;
	bool keyDownLastFrame;
	bool keyDownThisFrame;
	int snakeSize;  //this will tell how long the snake is. that way we can call up specific
					//sprites in draw.
	
	std::queue<Image*> snakeBody;
	Image* tailHolder; //when dequeueing need something to hold onto the tail
	Image* headHolder;  //when dequeueing need something to hold onto the head

	//create the rat
	TextureManager ratTexture;
	Image rat;

	int timer; //timer to know when to move the snake
public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
